﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;


namespace PFM.PageObjects
{
    public class LogOutPage
    {
        [FindsBy(How = How.CssSelector, Using = "span[class='uname']")]
        public IWebElement UsernameButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "div.b-popup-i>a")]
        public IWebElement LogOutButton { get; set; }

        public LogOutPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }
    }
}
