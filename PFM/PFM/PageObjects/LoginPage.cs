﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

namespace PFM.PageObjects
{
    public class LoginPage
    {
        [FindsBy(How = How.ClassName, Using = "enter")]
        public IWebElement LoginButton { get; set; }

        [FindsBy(How = How.Name, Using = "login")]
        public IWebElement UsernameField { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@type='password']")]
        public IWebElement PasswordField { get; set; }

        [FindsBy(How = How.CssSelector, Using = "input[type='submit']")]
        public IWebElement SubmitButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "span[class='uname']")]
        public IWebElement UsernameButton { get; set; }

        public LoginPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }   
    }
}
