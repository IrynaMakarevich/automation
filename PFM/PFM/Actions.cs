﻿using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using PFM.PageObjects;
using System;

namespace PFM
{
    public static class Actions
    {
        public static void FillLoginForm(LoginPage loginPage, string username, string password)
        {
            loginPage.LoginButton.Click();
            loginPage.UsernameField.SendKeys(username);
            loginPage.PasswordField.SendKeys(password);
            loginPage.SubmitButton.Click();
        }

        public static void ClickLogoutForm (LogOutPage logoutPage)
        {
            logoutPage.UsernameButton.Click();
            logoutPage.LogOutButton.Click();
        }
    }
}
