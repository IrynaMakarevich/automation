﻿using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using PFM.PageObjects;
using System;

namespace PFM.TestCases
{
    public class LoginTest
    {
        [TestCase("seleniumtests@tut.by", "123456789zxcvbn")]
        public void LogInTest(string username, string password)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "https://tut.by";

            var loginPage = new LoginPage(driver);
            
            Actions.FillLoginForm(loginPage, username, password);
            
            Assert.AreEqual(loginPage.UsernameButton.Text, "Selenium Test", "Result text is wrong");
            driver.Quit();
        }


    }
}
