﻿using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using PFM.PageObjects;
using System;

namespace PFM.TestCases
{
    class LogoutTest
    {
        [TestCase("seleniumtests@tut.by", "123456789zxcvbn")]
        public void LogOutTest(string username, string password)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "https://tut.by";

            var loginPage = new LoginPage(driver);
            Actions.FillLoginForm(loginPage, username, password);
            
            var logoutPage = new LogOutPage(driver);
            Actions.ClickLogoutForm(logoutPage);
            
            Assert.Throws<NoSuchElementException>(()=>driver.FindElement(By.CssSelector("span[class='uname']")));
            driver.Quit();
        }
        
    }
}
