﻿using OpenQA.Selenium;
using NUnit.Framework;

namespace AutomationTraining
{
    class Items
    {
        //login test
        public static By LoginButton = By.ClassName("enter");
        public static By UsernameField = By.Name("login");
        public static By PasswordField = By.XPath("//input[@type='password']");
        public static By SubmitButton = By.CssSelector("input[type='submit']");
        public static By UsernameButton = By.CssSelector("span[class='uname']");
        public static By ForgotPassword = By.LinkText("не помню");
        //iframe test
        public static By DefaultText = By.CssSelector("body.mce-content-body p");
        public static By Body = By.CssSelector("body.mce-content-body");
        public static By BoldButton = By.CssSelector("i.mce-ico.mce-i-bold");
        //alert tests
        public static By AlertButton = By.CssSelector("button[onclick='jsAlert()']");
        public static By ConfirmButton = By.CssSelector("button[onclick='jsConfirm()']");
        public static By PromptButton = By.CssSelector("button[onclick='jsPrompt()']");
        public static By Result = By.CssSelector("p#result");
        
    }
}
