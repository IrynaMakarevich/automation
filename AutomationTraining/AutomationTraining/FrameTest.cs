﻿using System;
using System.Threading;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;

namespace AutomationTraining
{
    [TestFixture]

    public class FrameTest
    {
        [Test]
        public void Test()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/iframe");
            driver.SwitchTo().Frame(driver.FindElement(By.Id("mce_0_ifr")));
            driver.FindElement(Items.DefaultText).Clear();
            driver.FindElement(Items.Body).SendKeys("Hello ");
            driver.SwitchTo().DefaultContent();
            driver.FindElement(Items.BoldButton).Click();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("mce_0_ifr")));
            driver.FindElement(Items.Body).SendKeys("world!");
            //check state of test
            Assert.AreEqual($"Hello {(char)65279}world!", driver.FindElement(Items.DefaultText).Text, "Result text is wrong");
            Thread.Sleep(2000);
            driver.Quit();
        }
    }
}
