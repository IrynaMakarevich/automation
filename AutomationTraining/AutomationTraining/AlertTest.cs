﻿using System;
using System.Threading;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;

namespace AutomationTraining
{
    [TestFixture]
    public class AlertTest
    {
        [Test]
        public void TestAlert()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/javascript_alerts");
            driver.FindElement(Items.AlertButton).Click();
            driver.SwitchTo().Alert().Accept();
            Assert.AreEqual(driver.FindElement(Items.Result).Text, "You successfuly clicked an alert", "Result text is wrong");
            Thread.Sleep(2000);
            driver.Quit();

        }
        [Test]
        public void TestComfirmationAccept()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/javascript_alerts");
            driver.FindElement(Items.ConfirmButton).Click();
            driver.SwitchTo().Alert().Accept();
            Assert.AreEqual(driver.FindElement(Items.Result).Text, "You clicked: Ok", "Result text is wrong");
            Thread.Sleep(2000);
            driver.Quit();

        }

        [Test]
        public void TestComfirmationDecline()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/javascript_alerts");
            driver.FindElement(Items.ConfirmButton).Click();
            driver.SwitchTo().Alert().Dismiss();
            Assert.AreEqual(driver.FindElement(Items.Result).Text, "You clicked: Cancel", "Result text is wrong");
            Thread.Sleep(2000);
            driver.Quit();

        }

        [Test]
        public void TestPrompt()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/javascript_alerts");
            driver.FindElement(Items.PromptButton).Click();
            var alert = driver.SwitchTo().Alert();
            alert.SendKeys("Test");
            alert.Accept();
            Assert.AreEqual(driver.FindElement(Items.Result).Text , "You entered: Test", "Result text is wrong");
            Thread.Sleep(2000);
            driver.Quit();
        }
    }
}
