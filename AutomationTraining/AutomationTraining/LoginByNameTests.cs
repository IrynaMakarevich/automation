﻿using System;
using System.Threading;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;

namespace AutomationTraining
{
    [TestFixture]
    public class LoginByNameTests
    {
        [Test]
        [TestCase ("seleniumtests@tut.by", "123456789zxcvbn")]
        [TestCase("seleniumtests2@tut.by", "123456789zxcvbn")]
        public void LoginByName( string login, string password)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://tut.by");
            //implicit waiter, will wait 5 sec  if element is not present immediately
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            
            driver.FindElement(Items.LoginButton).Click();
            driver.FindElement(Items.UsernameField).SendKeys(login);
            driver.FindElement(Items.PasswordField).SendKeys(password);
            driver.FindElement(Items.SubmitButton).Click();
            //explicit waiter waits 5 second while UsernameButton doesn't appear
            WebDriverWait UsernameWaiter = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            UsernameWaiter.Until(ExpectedConditions.ElementExists(Items.UsernameButton));
            //check state of test
            Assert.AreEqual(driver.FindElement(Items.UsernameButton).Text, "Selenium Test", "Result text is wrong");
            //Waiter which stops main flow execution on time in () in miliseconds (relates to emplicit waiters)
            Thread.Sleep(2000);
            driver.Quit();
        }
        
    }
}
